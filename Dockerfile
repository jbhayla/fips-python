FROM python:3.8-alpine

# Download packages as they're needed for cryptography which is used by twine
RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev && pip install twine && apk del gcc musl-dev python3-dev libffi-dev openssl-dev

RUN mkdir /code/
WORKDIR /code/

# Install dependencies
COPY requirements.txt /code/
RUN pip3 install --upgrade pip && pip3 install -r requirements.txt

# Copy source code over (only copy what you need, don't do " . ")
COPY setup.py /code/
COPY aws_fips_python/ /code/aws_fips_python/
COPY upload_to_server.sh /code/

# Install fips-python
RUN python3 setup.py install

CMD ["pytest"]