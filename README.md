# README #

This repository contains the code for the FIPS endpoint implementation for Python

### Environment setup
```
virtualenv -p python 3.6 dialog_engine_analytics_env
source dialog_engine_analytics_env/bin/activate
source ./set_local_env.sh
pip install -r requirements.txt
pip install .
```

### How to run tests
```
pytest
```
