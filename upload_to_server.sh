#!/bin/sh

echo "Uploading package to PyPI using twine"

# Clean the dist folder
rm -rf dist

python3 setup.py sdist bdist_wheel

if [ $? -eq 0 ]; then
    echo "Build Successful"
    twine upload --repository-url https://purecloud.jfrog.io/purecloud/api/pypi/inin-pypi/ -u $USERNAME -p $PASSWORD dist/*
else
    echo "Build Failed"
    exit 1
fi