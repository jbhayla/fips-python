import os
import unittest

import boto3
from parameterized import parameterized

from aws_fips_python.FipsEndpoints import GenesysBoto3Session


class TestEndpoints(unittest.TestCase):

    def setUp(self):
        os.environ['AWS_DEFAULT_REGION'] = 'us-east-2'
        self.endpoint = GenesysBoto3Session()

    @parameterized.expand([
        ('accessanalyzer', 'https://access-analyzer-fips.us-east-2.amazonaws.com'),
        ('acm', 'https://acm-fips.us-east-2.amazonaws.com'),
        ('acm-pca', 'https://acm-pca-fips.us-east-2.amazonaws.com'),
        ('apigateway', 'https://apigateway-fips.us-east-2.amazonaws.com'),
        ('athena', 'https://athena-fips.us-east-2.amazonaws.com'),
        ('backup', 'https://backup-fips.us-east-2.amazonaws.com'),
        ('batch', 'https://fips.batch.us-east-2.amazonaws.com'),
        ('clouddirectory', 'https://clouddirectory-fips.us-east-2.amazonaws.com'),
        ('cloudformation', 'https://cloudformation-fips.us-east-2.amazonaws.com'),
        ('cloudtrail', 'https://cloudtrail-fips.us-east-2.amazonaws.com'),
        ('codebuild', 'https://codebuild-fips.us-east-2.amazonaws.com'),
        ('codecommit', 'https://git-codecommit-fips.us-east-2.amazonaws.com'),
        ('codedeploy', 'https://codedeploy-commands-fips.us-east-2.amazonaws.com'),
        ('codepipeline', 'https://codepipeline-fips.us-east-2.amazonaws.com'),
        ('cognito-sync', 'https://cognito-sync-fips.us-east-2.amazonaws.com'),
        ('comprehend', 'https://comprehend-fips.us-east-2.amazonaws.com'),
        ('config', 'https://config-fips.us-east-2.amazonaws.com'),
        ('datasync', 'https://datasync-fips.us-east-2.amazonaws.com'),
        ('detective', 'https://api.detective-fips.us-east-2.amazonaws.com'),
        ('directconnect', 'https://directconnect-fips.us-east-2.amazonaws.com'),
        ('dms', 'https://dms-fips.us-east-2.amazonaws.com'),
        ('ds', 'https://ds-fips.us-east-2.amazonaws.com'),
        ('dynamodb', 'https://dynamodb-fips.us-east-2.amazonaws.com'),
        ('ebs', 'https://ebs-fips.us-east-2.amazonaws.com'),
        ('ec2', 'https://ec2-fips.us-east-2.amazonaws.com'),
        ('ecr', 'https://ecr-fips.us-east-2.amazonaws.com'),
        ('ecs', 'https://ecs-fips.us-east-2.amazonaws.com'),
        ('efs', 'https://elasticfilesystem-fips.us-east-2.amazonaws.com'),
        ('eks', 'https://fips.eks.us-east-2.amazonaws.com'),
        ('elasticache', 'https://elasticache-fips.us-east-2.amazonaws.com'),
        ('elasticbeanstalk', 'https://elasticbeanstalk-fips.us-east-2.amazonaws.com'),
        ('elb', 'https://elasticloadbalancing-fips.us-east-2.amazonaws.com'),
        ('emr', 'https://elasticmapreduce-fips.us-east-2.amazonaws.com'),
        ('es', 'https://es-fips.us-east-2.amazonaws.com'),
        ('events', 'https://events-fips.us-east-2.amazonaws.com'),
        ('firehose', 'https://firehose-fips.us-east-2.amazonaws.com'),
        ('fms', 'https://fms-fips.us-east-2.amazonaws.com'),
        ('fsx', 'https://fsx-fips.us-east-2.amazonaws.com'),
        ('glacier', 'https://glacier-fips.us-east-2.amazonaws.com'),
        ('glue', 'https://glue-fips.us-east-2.amazonaws.com'),
        ('groundstation', 'https://groundstation-fips.us-east-2.amazonaws.com'),
        ('guardduty', 'https://guardduty-fips.us-east-2.amazonaws.com'),
        ('inspector', 'https://inspector-fips.us-east-2.amazonaws.com'),
        ('iot', 'https://iot-fips.us-east-2.amazonaws.com'),
        ('iot-data', 'https://data.iot-fips.us-east-2.amazonaws.com'),
        ('iot-jobs-data', 'https://data.jobs.iot-fips.us-east-2.amazonaws.com'),
        ('iotfleethub', 'https://api.fleethub.iot-fips.us-east-2.amazonaws.com'),
        ('iotsecuretunneling', 'https://api.tunneling.iot-fips.us-east-2.amazonaws.com'),
        ('kafka', 'https://kafka-fips.us-east-2.amazonaws.com'),
        ('kinesis', 'https://kinesis-fips.us-east-2.amazonaws.com'),
        ('kinesisanalytics', 'https://kinesisanalytics-fips.us-east-2.amazonaws.com'),
        ('kms', 'https://kms-fips.us-east-2.amazonaws.com'),
        ('lambda', 'https://lambda-fips.us-east-2.amazonaws.com'),
        ('license-manager', 'https://license-manager-fips.us-east-2.amazonaws.com'),
        ('logs', 'https://logs-fips.us-east-2.amazonaws.com'),
        ('macie2', 'https://macie2-fips.us-east-2.amazonaws.com'),
        ('medialive', 'https://medialive-fips.us-east-2.amazonaws.com'),
        ('mq', 'https://mq-fips.us-east-2.amazonaws.com'),
        ('opsworks', 'https://opsworks-cm-fips.us-east-2.amazonaws.com'),
        ('polly', 'https://polly-fips.us-east-2.amazonaws.com'),
        ('quicksight', 'https://fips-us-east-2.quicksight.aws.amazon.com'),
        ('ram', 'https://ram-fips.us-east-2.amazonaws.com'),
        ('rds', 'https://rds-fips.us-east-2.amazonaws.com'),
        ('rds-data', 'https://rds-data-fips.us-east-2.amazonaws.com'),
        ('redshift', 'https://redshift-fips.us-east-2.amazonaws.com'),
        ('rekognition', 'https://rekognition-fips.us-east-2.amazonaws.com'),
        ('resource-groups', 'https://resource-groups-fips.us-east-2.amazonaws.com'),
        ('s3', 'https://s3-fips.us-east-2.amazonaws.com'),
        ('sagemaker', 'https://api-fips.sagemaker.us-east-2.amazonaws.com'),
        ('sagemaker-runtime', 'https://runtime-fips.sagemaker.us-east-2.amazonaws.com'),
        ('secretsmanager', 'https://secretsmanager-fips.us-east-2.amazonaws.com'),
        ('servicecatalog', 'https://servicecatalog-fips.us-east-2.amazonaws.com'),
        ('servicediscovery', 'https://servicediscovery-fips.us-east-2.amazonaws.com'),
        ('sms', 'https://sms-fips.us-east-2.amazonaws.com'),
        ('snowball', 'https://snowball-fips.us-east-2.amazonaws.com'),
        ('sns', 'https://sns-fips.us-east-2.amazonaws.com'),
        ('sqs', 'https://sqs-fips.us-east-2.amazonaws.com'),
        ('ssm', 'https://ssm-fips.us-east-2.amazonaws.com'),
        ('stepfunctions', 'https://states-fips.us-east-2.amazonaws.com'),
        ('storagegateway', 'https://storagegateway-fips.us-east-2.amazonaws.com'),
        ('sts', 'https://sts-fips.us-east-2.amazonaws.com'),
        ('swf', 'https://swf-fips.us-east-2.amazonaws.com'),
        ('textract', 'https://textract-fips.us-east-2.amazonaws.com'),
        ('transcribe', 'https://fips.transcribe.us-east-2.amazonaws.com'),
        ('transfer', 'https://transfer-fips.us-east-2.amazonaws.com'),
        ('translate', 'https://translate-fips.us-east-2.amazonaws.com'),
        ('waf', 'https://waf-regional-fips.us-east-2.amazonaws.com'),
        ('wafv2', 'https://wafv2-fips.us-east-2.amazonaws.com'),
        ('xray', 'https://xray-fips.us-east-2.amazonaws.com')
    ])
    def test_service(self, service_name, endpoint_url):
        client = self.endpoint.client(service_name)
        boto3_client = boto3.client = client
        self.assertEqual(endpoint_url, boto3_client.meta.endpoint_url)
