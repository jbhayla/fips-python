import json
import os
from os.path import dirname, join

import boto3
from boto3.session import Session as Boto3Session

FILE_PATH = os.path.dirname(os.path.abspath(__file__))
BUILTIN_DATA_PATH = os.path.join(FILE_PATH, 'data')

FIPS_ENABLED_REGIONS = ['us-east-1', 'us-east-2']
FIPS = 'fips'


class GenesysBoto3Session(Boto3Session):

    def client(self, service_name, region_name=None, api_version=None, use_ssl=True, verify=None, endpoint_url=None,
               aws_access_key_id=None, aws_secret_access_key=None, aws_session_token=None, config=None):
        endpoint_url = self._get_fips_region_endpoint_if_enabled(service_name, region_name, endpoint_url)
        return super().client(service_name, region_name, api_version, use_ssl, verify, endpoint_url, aws_access_key_id,
                              aws_secret_access_key, aws_session_token, config)

    def resource(self, service_name, region_name=None, api_version=None, use_ssl=True, verify=None, endpoint_url=None,
                 aws_access_key_id=None, aws_secret_access_key=None, aws_session_token=None, config=None):
        endpoint_url = self._get_fips_region_endpoint_if_enabled(service_name, region_name, endpoint_url)
        return super().resource(service_name, region_name, api_version, use_ssl, verify, endpoint_url,
                                aws_access_key_id, aws_secret_access_key, aws_session_token, config)

    def _load_json_file(self, file_path):
        with open(file_path) as file:
            json_info = json.loads(file.read())
        return json_info

    def _get_fips_region_endpoint_if_enabled(self, service_name, user_defined_region_name, endpoint_url):
        path = join(dirname(__file__), "data/endpoints-scraped.json")
        service_map = self._load_json_file(path)
        if (user_defined_region_name is None and self.region_name in FIPS_ENABLED_REGIONS) or user_defined_region_name in FIPS_ENABLED_REGIONS:
            regions = service_map[service_name]
            if self.region_name in regions:
                endpoint_host_name = regions[self.region_name]
                return "https://" + endpoint_host_name
        return endpoint_url


session = GenesysBoto3Session()
boto3.client = session.client
boto3.resource = session.resource
