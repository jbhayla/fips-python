"""
Standard setup.py for easy install
For development do
# python setup.py develop
for install and
# python setup.py develop -u
for uninstall
"""
from setuptools import setup

pkgs = ['aws_fips_python', 'aws_fips_python.data']
version = "0.2.11"

setup(
    name="test_fips_python",
    version=version,
    zip_safe=False,
    packages=pkgs,
    package_dir={'': '.'},
    install_requires=[
        "boto3>=1.12.21"
    ],
    include_package_data=True
)
